;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GWB_FRAG parameter file:
;; NOTE: do NOT delete or add any lines in this parameter file!
;; Fragmentation analysis at up to 10 user-selected Fixed Observation Scales (FOS):
;; FAC (Foreground Area Clustering); FAD (Foreground Area Density)
;;
;; Options:
;; FAC(FAD)5/6: per-pixel clustering (density), color-coded into 5/6 fragmentation classes
;; FAC(FAD)-APP2/5: average per-patch clustering (density), color-coded into 2/5 classes
;; 
;; Input image requirements: 1b-background, 2b-foreground, optional: 
;;    0b-missing, 3b-special background, 4b-non-fragmenting background
;;
;; FRAG will provide one (1) image per observation scale and summary statistics.
;; (method: FAC or FAD; reporting at pixel or patch (APP) level; # of reporting classes: 2, 5, 6)
;; Please specify entries at lines 32-36 ONLY using the following options:
;; line 32: FAC_5 (default)  or  FAC_6, FAC-APP_2, FAC_APP_5, FAD_5, FAD_6, FAD-APP_2  or  FAD-APP_5
;; line 33: Foreground connectivity: 8 (default) or 4
;; line 34: pixel resolution [meters]
;; line 35: up to 10 window sizes (unit: pixels, uneven within [3, 501] ) in increasing order and separated by a single space.
;; line 36: high-precision: 1-float precision  (default)  or 0-rounded byte
;; line 37: statistics: 0 (default) or 1 (add summary statistics)
;;
;; an example parameter file using the default settings:
;; FAC_5
;; 8
;; 100
;; 27
;; 1
;; 0
****************************************************************************
FAC_5
8
100
27
1
1
****************************************************************************
